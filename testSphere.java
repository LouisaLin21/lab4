import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class testSphere {
    Sphere a = new Sphere(3.0);

    @Test
    public void testgetVolume() {
        assertEquals(84.78, a.getVolume());
    }

    @Test
    public void testgetSurfaceArea() {
        assertEquals(113.04, a.getSurfaceArea());
    }
}
