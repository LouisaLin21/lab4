//Louisa Yuxin Lin 1933472
public class Sphere {
    private double radius;
    private double pi = 3.14;

    public Sphere(double r) {
        this.radius = r;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getVolume() {
        return Math.pow(this.radius, 3) * pi;
    }

    public double getSurfaceArea() {
        double surfaceArea = 4 * pi * Math.pow(this.radius, 2);
        return surfaceArea;
    }
}