//Qiuning Liang 2037000
public class Cylinder{

    private double height;
    private double radius;
    final double x = 3.14;

    public Cylinder(double height,double radius){
        this.height=height;
        this.radius=radius;
    }

    public double getHeight(){
        return this.height;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getVolume(){
        double volume = this.x * Math.pow(this.radius,2) * this.height;
        return volume;
    }

    public double getSurfaceArea(){
        double area = 2 * this.x * Math.pow(this.radius,2) + 2 * this.x * this.radius * this.height;
        return area;
    }

}