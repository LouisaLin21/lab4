import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

/*Louisa Yuxin Lin 1933472*/
public class TestCylinder {
    Cylinder c = new Cylinder(2.0, 5.0);
    Cylinder b = new Cylinder(2, 5);


    @Test
    public void testGetHeight() {
        assertEquals(2.0, c.getHeight());
    }

    @Test
    public void testgetRadius() {
        assertEquals(5.0, c.getRadius());
    }

    @Test
    public void testGetVolume() {
        assertEquals(157.0, c.getVolume());
    }

    @Test
    public void testGetSurfaceArea() {
        assertEquals(219.8, c.getSurfaceArea());
    }
}
